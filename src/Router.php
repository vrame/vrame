<?php

namespace Vrame;

class Router {

  private static $definition = [];

  public function __construct() {
  }

  public static function load($defs) {
    self::$definition = $defs;
  }

  public static function find($url) {
    $url = '/' . preg_replace('/^\/+/', '', $url);

    foreach (self::$definition as $rgxp => $urlRgxp) {
      if (preg_match(self::prepare($rgxp), $url, $matches)) {
        if (is_array($urlRgxp)) {
          return [
            'controller'  => $urlRgxp[0],
            'action'      => $urlRgxp[1],
            'params'      => array_slice($matches, 1)
          ];
        }

        return preg_replace(self::prepare($rgxp), $urlRgxp, $url);
      }
    }
    return false;
  }

  private static function prepare($rgxp) {
    return '/' . str_replace('/', "\\/", $rgxp) . '/i';
  }
}

?>