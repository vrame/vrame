<?php

namespace Vrame;

use \Exception;

class Dispatcher {
  private $_request;
  private $request;
  private static $_instance;
  private static $_error_handler;

  public static function dispatch(ErrorHandler $handler=null) {
    self::errorHandler($handler);
    self::getInstance(@$_REQUEST['__request'])
      ->parseRequest();
  }

  private static function getInstance($request) {
    if (! self::$_instance) {
      self::$_instance = new self();

      $request = preg_replace('/\/+$/', '', $request);
      self::$_instance->_request = $request;
    }

    return self::$_instance;
  }

  private static function errorHandler(ErrorHandler $handler = null) {
    if ($handler) {
      self::$_error_handler = $handler;
    } else {
      return self::$_error_handler;
    }
  }

  public function parseRequest() {
    $this->request = APP::parseUrl($this->_request);
    try {
      APP::getInstance();

      $className = APP::camelcase($this->request->controller);
      $ctrlName = "App\\Controller\\{$className}";
      if (! class_exists($ctrlName)) {
        $this->handleError("Controller `{$className}` is missing", 404);
      }
      $ctrl = new $ctrlName($this->request);

      if (method_exists($ctrl, $this->request->action)) {
        $ctrl->beforeFilter();
        call_user_func_array(array($ctrl, $this->request->action), $this->request->params);
        if ($ctrl->autoRender && !$ctrl->rendered) {
          $ctrl->render();
        }
      } else {
        $this->handleError(sprintf("Action `%s` in Controller `%s` is missing",
                                  $this->request->action,
                                  $this->request->controller)
                          , 404);
      }
    } catch (Exception $e) {
      if ($eh = self::errorHandler()) {
        $eh->exception($e, $this);
      } else {
        $ctrl = new Controller($this->request);
        $message = $e->getMessage();
        $ctrl->http_error(404, get_class($e), "<h1>{$message}</h1>");
      }
    }
  }

  public function controller($name) {
    $klass = "App\\Controller\\" . APP::camelcase($name);
    return new $klass($this->request);
  }

  private function handleError($message, $code=500, $file=null, $line=null) {
    if ($eh=self::errorHandler()) {
      $eh->error((object)[
        'message' => $message,
        'code'    => $code,
        'file'    => $file,
        'line'    => $line
      ], $this);
    } else {
      throw new Exception($message);
    }
  }
}

?>
