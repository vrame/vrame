<?php

namespace Vrame;

use Symfony\Component\Console\Application,
    Vrame\Cli\MigrateCommand,
    Vrame\Cli\MigrationGenerateCommand,
    Vrame\Cli\MigrationRollbackCommand,
    Vrame\Cli\GenerateSkeletonCommand;

class Cli extends Application {
  public function __construct() {
    $data     = file_get_contents(__DIR__ . "/../composer.json");
    $json     = json_decode($data);

    parent::__construct($json->name, $json->version);
    $this->add(new MigrateCommand());
    $this->add(new MigrationGenerateCommand());
    $this->add(new MigrationRollbackCommand());
    $this->add(new GenerateSkeletonCommand());
  }
}

?>
