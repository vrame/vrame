<?php

namespace Vrame\Cli;

class MigrationRollbackCommand extends BaseMigrationCommand {
  protected $name         = 'db:rollback';
  protected $description  = 'rollsback migration';

  protected function main() {
    $this->out->writeln($this->phinx("rollback"));
  }
}

?>
