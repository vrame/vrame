<?php

namespace Vrame\Cli;

use Symfony\Component\Console\Input\InputArgument;

class GenerateSkeletonCommand extends BaseGenerateCommand {
  protected $name         = 'generate:skeleton';
  protected $description  = 'generates whole skeleton(controller,model,view)';
  protected $arguments    = [
    ['name', InputArgument::REQUIRED, 'skeleton name']
  ];

  protected function main($name) {
    $this->out->writeln("Generate {$name}");
  }
}
