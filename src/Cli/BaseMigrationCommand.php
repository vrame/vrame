<?php

namespace Vrame\Cli;

abstract class BaseMigrationCommand extends BaseCommand {
  const PHINX = "robmorgan/phinx";

  protected function phinx($cmd) {
    $phinx =  "vendor/" . self::PHINX . "/bin/phinx";
    if (! file_exists($phinx)) {
      throw new Exception(self::PHINX . " is missing");
    }
    $out = "";
    exec("{$phinx} {$cmd}", $out);
    return $out;
  }
}

?>
