<?php

namespace Vrame\Cli;

class MigrateCommand extends BaseMigrationCommand {
  protected $name         = 'db:migrate';
  protected $description  = 'runs migrations';

  protected function main() {
    $this->out->writeln($this->phinx("migrate"));
  }
}

?>
