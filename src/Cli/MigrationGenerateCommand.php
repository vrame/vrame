<?php

namespace Vrame\Cli;

use Symfony\Component\Console\Input\InputArgument;

class MigrationGenerateCommand extends BaseMigrationCommand {
  protected $name         = 'db:migration';
  protected $description  = 'generates migration';
  protected $arguments    = [
    ['name', InputArgument::REQUIRED, 'Migration name']
  ];

  protected function main($name) {
    $this->out->writeln($this->phinx("create {$name}"));
  }
}

?>
