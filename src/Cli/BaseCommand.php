<?php

namespace Vrame\Cli;

use Symfony\Component\Console\Command\Command,
    Symfony\Component\Console\Input\InputInterface,
    Symfony\Component\Console\Output\OutputInterface;

abstract class BaseCommand extends Command {
  protected $name         = '';
  protected $description  = '';
  protected $arguments    = [];
  protected $options      = [];

  protected $in;
  protected $out;

  protected function configure() {
    $this->setName($this->name)
      ->setDescription($this->description);

    foreach ($this->arguments as $arg) {
      $this->addArgument(...$arg);
    }

    foreach ($this->options as $opt) {
      $this->addOption(...$opt);
    }
  }

  protected function execute(InputInterface $in, OutputInterface $out) {
    $this->in   = $in;
    $this->out  = $out;

    $args = [];
    foreach ($this->arguments as $arg) {
      $args[] = $this->in->getArgument($arg[0]);
    }
    $this->main(...$args);
  }

  protected function main() {
  }
}
