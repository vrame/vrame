<?php

namespace Vrame;

use Vrame\Exception\ModelMissingException,
    \mysqli,
    \StdClass,
    \Exception;

class Model {

  public static $LIMIT = 10;

  public  $table = '';
  public  $name  = '';

  private $connection;
  private $error;
  private $sql;

  public $q_join = [];
  public $q_fields;
  public $q_where;
  public $q_order;
  public $q_group;
  public $q_having;
  public $q_limit;
  public $q_offset;
  public $q_page;

  public $fields= '*';
  public $where = [];
  public $order = false;
  public $group = false;
  public $limit = false;
  public $page  = 1;
  public $having= false;

  public $validate        = [];
  public $validateErrors  = [];

  private $alias;
  private $meta;

  private static $inst = [];
  private static function getInstance() {
    $className = get_called_class();
    if (! isset(self::$inst[$className])) {
      $className = get_called_class();
      $modelName = preg_replace('/Model$/', '', $className);
      self::$inst[$className] = new $className($modelName);
    }
    $i = self::$inst[$className];
    $i->q_fields = null;
    $i->q_where  = null;
    $i->q_order  = null;
    $i->q_group  = null;
    $i->q_having = null;
    $i->q_limit  = null;
    $i->q_page   = null;
    $i->q_join   = null;
    $i->q_offset = null;

    return $i;
  }

  private function __construct($name) {
    $dbConfig = APP::config('db');

        // create DB connection
    $this->connection = new mysqli( $dbConfig['host'],
                                   $dbConfig['username'],
                                   $dbConfig['password'],
                                   $dbConfig['database'],
                                   isset($dbConfig['port'])  ? $dbConfig['port'] : 3306 );
    if ($error = $this->connection->connect_error) {
      throw new Exception($error);
    }

        // set encoding
    $encoding = isset($dbConfig['encoding']) ? $dbConfig['encoding'] : false;
    $this->execute("SET NAMES " . ($encoding ? $encoding : 'utf8'));

        // save name
    $name = explode("\\", $name);
    $name = end($name);
    $this->name = $name;

        // set table name if it's empty
    if (! $this->table) {
      $this->table = APP::underscore($name);
    }
  }

  public static function options_for_select($value=null, $label=null, $conditions=[]) {
    $value = $value ? $value : 'id';
    $label = $label ? $label : 'name';

    $options = [];
    $data = self::build()
    ->select([$value, $label])
    ->where($conditions)
    ->query();
    foreach($data as $row) {
      $options[$row->$value] = $row->$label;
    }
    return $options;
  }

  public static function build() {
    $inst = self::getInstance();
    $inst->q_fields = $inst->fields;
    $inst->q_where  = $inst->where;
    $inst->q_order  = $inst->order;
    $inst->q_group  = $inst->group;
    $inst->q_limit  = $inst->limit;
    $inst->q_page   = $inst->page;
    $inst->q_having = $inst->having;

    return $inst;
  }

  public function join($modelName, $conditions=null, $type='INNER', $alias=null) {
    if (! $alias) {
      $alias = $modelName;
    }
    if (! $conditions) {
      $self = $this->alias();
      $conditions = "`{$alias}`.id = `{$self}`.".strtolower($modelName)."_id";
    }
    $this->q_join[$alias] = [
                              'model'       => $modelName,
                              'type'        => $type,
                              'conditions'  => $conditions
                            ];

    return $this;
  }

  public function joinSQL($alias, $sql) {
    $this->q_join[$alias] = $sql;
    return $this;
  }

  public function select($fields) {
    $this->q_fields = $fields;
    return $this;
  }

  public function where($where) {
    $this->q_where = $where;
    return $this;
  }

  public function order($order) {
    $this->q_order = $order;
    return $this;
  }

  public function group($group) {
    $this->q_group = $group;
    return $this;
  }

  public function having($having) {
    $this->q_having = $having;
    return $this;
  }

  public function limit($limit, $offset=null) {
    $this->q_limit = $limit;
    if ($offset != null) $this->q_offset = $offset;

    return $this;
  }

  public function page($page, $limit=null) {
    $this->q_page = (int)$page;
    $this->q_limit = $limit===null ? self::$LIMIT : $limit;
    return $this;
  }

  public function createRecord($data=[]) {
    $fields = $this->describe();
    $obj = new StdClass;
    foreach ($fields as $f) {
      $name = $f->Field;
      if (isset($data[$name])) {
        $v = $data[$name];
      } else {
        $v = $f->Default;
        if ( in_array(strtolower($v),  ['current_timestamp', 'now'])) {
          $v = date('Y-m-d H:i:s');
        } elseif(is_null($v) && strtolower($f->Null)=='no') {
          if(preg_match('/^enum/i', $f->Type)) {
            preg_match_all('/[\'"]([^,]+)[\'"]/', $f->Type, $matches);
            $v = $matches[1][0];
          }
        }
      }
      $obj->{$f->Field} = $v;
    }
    return $obj;
  }

  public static function create($data=[]) {
    $record = self::getInstance()
    ->createRecord($data);
    if (! empty($data)) {
      foreach ($data as $k=>$v) {
        $record->$k = $v;
      }
    }

    return $record;
  }

  public static function get($id, $fields="*") {
    if (is_array($fields)) {
      $fields = implode(", ", $fields);
    }

    $inst = self::getInstance();
    if ($result = $inst->query_by_sql("SELECT {$fields} FROM {$inst->table} WHERE id={$id}")) {
      return (object)$result[0];
    }

    return false;
  }

  public function query($sql=null) {
    if ($sql !== null) {
      return $this->query_by_sql($sql);
    }

    $sql = [];

        // SELECT
    $q_fields = is_array($this->q_fields) ? $this->q_fields : [$this->q_fields];
    if (! empty($this->q_join)) {
      $q_fields = $this->labelizeSelect($q_fields);
    }
    $sql[] = "SELECT " . implode(', ', $q_fields);

        // FROM
    $sql[] = "FROM `{$this->table}` as `" . $this->alias() . "`";

        // JOIN
    if (! empty($this->q_join)) {
      foreach ($this->q_join as $alias => $data) {
        if (is_array($data)) {
          try {
            $inst = $this->loadModel($data['model']);
            $sql[] = $data['type']
            ." JOIN `{$inst->table}` as `{$alias}`"
            ." ON " . $data['conditions'];
          } catch (Exception\ModelMissingException $e) {
            $sql[] = $data['type'] . " JOIN {$alias} ON ".$data['conditions'];
          }
        } else {
          $sql[] = $data;
        }
      }
    }

    // WHERE
    if (! empty($this->q_where)) {
      $sql[] = "WHERE " . $this->parseConditions($this->q_where);
    }

    // GROUP
    if ($this->q_group) {
      $sql[] = "GROUP BY {$this->q_group}";
    }

    // HAVING
    if ($this->q_having) {
      $sql[] = "HAVING {$this->q_having}";
    }

    // ORDER
    if ($this->q_order) {
      $sql[] = "ORDER BY {$this->q_order}";
    } elseif($this->order) {
      $sql[] = "ORDER BY {$this->order}";
    }

    // LIMIT
    if ($this->q_limit) {
      $limit  = (int)$this->q_limit;
      $offset = isset($this->q_offset) ? $this->q_offset : ($this->q_page-1) * $limit;
      $sql[] = "LIMIT {$offset}, {$limit}";
    }

    return $this->query_by_sql(implode("\n", $sql));
  }

  public function query_by_sql($sql) {
    $this->sql = $sql;
    $result = $this->connection->query($sql);
    if ($result) {
      $this->errorReset();
      $ret = [];
      while ($row = $result->fetch_assoc()) {
        $keys = array_keys($row);
        if (strstr($keys[0], ".")) {
          $tmp = [];
          foreach ($row as $k=>$v) {
            list($model, $field) = explode(".", $k, 2);

            if (! isset($tmp[$model])) {
              $tmp[$model] = new StdClass();
            }
            $tmp[$model]->$field = $v;
          }
          $row = $tmp;
        }
        $ret[] = (object)$row;
      }
      $result->free();
      return $ret;
    } else {
      $this->errorLoad();
    }
    return false;
  }

  public function first($sql=null) {
    $result = $this
    ->limit(1,0)
    ->query($sql);
    return @$result[0];
  }

  public function count($field='*', $limit=null) {
    if (!$limit) $limit = self::$LIMIT;
    $cnt = $this->select(["COUNT({$field}) cnt"])
                ->limit(false)
                ->query();
    if (! $cnt) {
      return 0;
    }
    $cnt = $cnt[0]->cnt;

    $offset = 0;
    if ($this->q_page > 0) {
      $offset = ($this->q_page - 1) * $limit;
    }
    $cnt = $cnt > $offset ? $cnt - $offset : 0;

    return $cnt;
  }

  public function validateData($data) {
    // flatten rules
    $rules = $this->parseValidationRule($this->validate);

    $status = true;
    foreach ($rules as $r) {
      $rule    = $r['rule'];
      $field   = $r['field'];
      $message = $r['message'];

      if (method_exists($this, $rule)) {
        $status1 = $this->$rule(@$data[$field], $data);
        if (! $status1) {
          $this->validateErrors[$field] = sprintf($message, $field);
        }
      } else if (preg_match('/^\/.+/', $rule)) {
        $status1 = preg_match($rule, @$data[$field]);
        if (! $status1) {
          $this->validateErrors[$field] = sprintf($message, $field);
        }
      } else {
        $status1 = false;
        $this->validateErrors[$field] = "Invalid validation rule `{$rule}`";
      }
      $status = $status && $status1;
    }

    return $status;
  }

  private function parseValidationRule($r, $forceField=false) {
    $defaultMessage = "Field %s is invalid.";

    $rules = [];
    foreach($r as $field=>$v) {
      if ($forceField) {
        $field = $forceField;
      }

      if(is_array($v)) {
        if (isset($v[0])) {
          $rules = array_merge($rules, $this->parseValidationRule($v, $field));
        } else {
          $rules[] =  [
                        'field'   => $field,
                        'rule'    => @$v['rule'],
                        'message' => isset($v['message']) ? $v['message'] : sprintf($defaultMessage, $field)
                      ];
        }
      } else {
        $rules[]  = [
                      'field'   => $field,
                      'rule'    => $v,
                      'message' => sprintf($defaultMessage, $field)
                    ];
      }
    }
    return $rules;
  }

  public static function update($fields, $conditions) {
    $inst = self::getInstance();
    $fields = (array)$fields;

    $sql = "UPDATE {$inst->table} ".
    "SET ";
    $segments = '';
    foreach ($fields as $k=>$v) {
      $segments .= $inst->conditionsSegment($k, $v, ", ", false, true);
    }
    $sql .= preg_replace('/^\s*,\s*/', '', $segments)." ";

    $sql .= "WHERE " . $inst->parseConditions($conditions);

    return $inst->execute($sql);
  }

  public static function insert($data) {
    $data = (array)$data;

    $inst = self::getInstance();
    if (! $inst->validateData($data)) {
      $inst->errorLoad([__('Invalid input data.')]);
      return false;
    }

    $fields = array_keys($data);
    $values = array_values($data);
    $sql = "INSERT INTO {$inst->table}(`".implode('`, `', $fields)."`) ".
    "VALUES(";
    foreach ($values as $value) {
      $sql .= substr($inst->conditionsSegment('', $value, ',', false), 8).", ";
    }
    $sql = preg_replace('/,\s*$/', '', $sql).")";

    return $inst->execute($sql);
  }

  public static function save($data, $idField='id', $skipValidation=false) {
    $inst = self::getInstance();
    $arrData = (array)$data;
    if (!$skipValidation && !$inst->validateData($arrData)) {
      $inst->errorLoad([__('Invalid input data.')]);
      return false;
    }

    $id = isset($arrData[$idField]) ? intval($arrData[$idField]) : null;
    unset($arrData[$idField]);
    if ($id>0) {
      return self::update($arrData, [$idField => $id]);
    } else {
      if (self::insert($arrData)) {
        $id = self::lastInsertId();
        if (is_array($data)) {
          $data[$idField] = $id;
        } else {
          $data->$idField = $id;
        }
        return $data;
      }
      return false;
    }
  }

  public static function delete($conditions) {
    $inst = self::getInstance();
    $sql = "DELETE FROM {$inst->table} WHERE " . $inst->parseConditions($conditions);
    return $inst->execute($sql);
  }

  public static function validationErrors() {
    return self::getInstance()->validateErrors;
  }

  public function execute($sql) {
    $this->sql = $sql;
    if (! $res = $this->connection->query($sql)) {
      $this->errorLoad();
    }
    return $res ? true : false;
  }

  public static function beginTransaction() {
    $inst = self::build();
    return $inst->connection->begin_transaction() ? $inst : false;
  }

  public function commit() {
    return $this->connection->commit();
  }

  public function rollback() {
    return $this->connection->rollback();
  }

  public function setOrder($order) {
    $this->order = $this->q_order = $order;
    return $this;
  }

  public function lastQuery() {
    return $this->sql;
  }

  public static function lastInsertId() {
    return self::getInstance()
    ->connection->insert_id;
  }

  public static function sql() {
    return self::getInstance()->sql;
  }

  public static function error() {
    $inst  = self::getInstance();
    $error = $inst->error;
    if ($error) {
      $error->sql = $inst->sql;
    }
    return $error;
  }

  protected function errorLoad($errors = null) {
    if ($errors === null) {
      $this->error = (object)[
                              'code'    => $this->connection->errno,
                              'message' => $this->connection->error
                            ];
    } else {
      $this->error = (object)[
                              'message' => implode(' ', $errors)
                            ];
    }
  }

  protected function errorReset() {
    $this->error = false;
  }

  private function parseConditions($conditions, $glue='AND') {
    $where = '';

    foreach ($conditions as $k=>$v) {
      if (in_array(strtoupper($k), ['AND', 'OR'])) {
        $where .= " {$glue} (".$this->parseConditions($v, strtoupper($k)) . ")";
      } else {
        $where .= $this->conditionsSegment($k, $v, $glue);
      }
    }

    return preg_replace('/^\s*'.$glue.'\s*/i', '', trim($where));
  }

  private function conditionsSegment($field, $value, $glue = 'AND', $enclose=true, $set=false) {
    $encS = '(';
    $encE = ')';
    if (! $enclose) {
      $encS = $encE = '';
    }

    if (strstr($field, ".")) {
      $field = str_replace(".", "`.`", $field);
    }

    $sql = " {$glue} {$encS}" . (is_numeric($field) ? "" : "`{$field}`");

    $comp = "=";

    $isFieldComp = function($fld) {
      preg_match('/[!<=>]+\s*$/', $fld, $matches);
      return empty($matches) ? false : $matches[0];
    };

    if (is_numeric($value)) {
      if ($match = $isFieldComp($field)) {
        $field = trim(preg_replace('/[!<>]+=?\s*$/i', '', $field));
        $sql = " $glue {$encS}`{$field}` {$match} {$value}";
      } else {
        $sql .= " {$comp} {$value}";
      }

    } elseif(is_bool($value)) {
      $sql .= " {$comp} " . ($value ? 1 : 0);

    } elseif(is_null($value)) {
      $sql .= $set ? " = NULL" : " IS NULL";

    } elseif(is_array($value)) {
      foreach ($value as &$v) {
        $v = $this->escape($v);
      }

      if (preg_match('/ between$/i', $field)) {
        $sql = preg_replace('/ between`$/i', '`', $sql)
        .  " BETWEEN '" . $value[0] . "' AND '" . $value[1] . "'";
      } else {
        $sql .= " IN ('".implode("','", $value)."')";
      }

    } else {
      if (preg_match('/(NOT\s)?LIKE\s*$/i', $field, $matches)) {
        $field = trim(preg_replace('/(NOT\s)?LIKE\s*$/i', '', $field));
        $sql   = " {$glue} {$encS}`{$field}` ". $matches[0] ." '" . $this->escape($value) . "'";

      } elseif($matches=$isFieldComp($field)) {
        $field = trim(preg_replace('/[!<>]+=?\s*$/i', '', $field));

        if (! is_numeric($value)) {
          $value = "'" . $this->escape($value) . "'";
        }

        $sql = " {$glue} {$encS}`{$field}` " . $matches[0] . " " . $value;

      } elseif(is_numeric($field)) {
        $sql .= "$value";
      } else {
        $sql .= " = '" . $this->escape($value) . "'";
      }
    }
    return $sql . $encE;
  }

  private function alias() {
    if (! $this->alias) {
      $this->alias = $this->name;
    }
    return $this->alias;
  }

  private function labelizeSelect($fields, $as=false, $alias=null) {
    $lFields = [];

    foreach($fields as $field) {
      if (strstr($field, '.')) {
        $lFields[] = $field;
      } elseif ($field == '*') {
        $desc = array_map(function($f){
          return $f->Field;
        }, $this->describe());

        $lFields = array_merge($lFields, $this->labelizeSelect($desc, true));
        if (! empty($this->q_join)) {
          foreach ($this->q_join as $alias => $data) {
            if (is_array($data)) {
              try {
                $inst = $this->loadModel($data['model']);
                $descJoin = array_map(function($f){
                  return $f->Field;
                }, $inst->describe());
                $lFields = array_merge($lFields, $inst->labelizeSelect($descJoin, true, $alias));
              } catch (ModelMissingException $e) {
                //void
              }
            } else {
              //TODO
            }
          }
        }
      } else {
        $alias = isset($alias) ? $alias : $this->alias();
        $f = $alias.".{$field}";
        $lFields[] = $f . ($as ? " as `{$f}`" : '');
      }
    }

    return $lFields;
  }

  private function describe() {
    if (! $this->meta) {
      $this->meta = $this->query("DESC {$this->table}");
    }

    return $this->meta;
  }

  private function loadModel($name) {
    $class_name = "App\\Model\\{$name}";
    if (! class_exists($class_name)) {
      throw new ModelMissingException($name);
    }

    return $class_name::getInstance();
  }

  public function escape($string) {
    return $this->connection->real_escape_string($string);
  }
}

?>
