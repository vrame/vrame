<?php

namespace Vrame;

use \SessionHandlerInterface;

class SessionHandler implements SessionHandlerInterface {

  public $table = 'sessions';
  public $key   = 'session_id';

  private $app;
  private $db_config;

  public function __construct() {
    session_set_save_handler($this);
  }

  public function start() {
    session_start();
    $this->db_config = APP::config('db');
  }

  public function open($save_path, $session_name) {
    return true;
  }

  public function close() {
    return true;
  }

  public function read($session_id) {
    $sql = "SELECT data FROM {$this->table} WHERE {$this->key} = '{$session_id}'";
    $session = $this->db_first($sql);
    if ($session) {
      return $session->data;
    }

    return '';
  }

  public function write($session_id, $data) {
    $data2 = $this->db_escape($data);
    $sql = "INSERT INTO {$this->table}({$this->key}, data)".
           "VALUES ('{$session_id}', '{$data2}') ".
           "ON DUPLICATE KEY UPDATE ".
            "data='{$data2}', updated=NOW()";
    return !!$this->db_exec($sql);
  }

  public function destroy($session_id) {
    $sql = "DELETE FROM {$this->table} WHERE {$this->key} = '{$session_id}'";
    return !!$this->db_exec($sql);
  }

  public function gc($lifetime) {
    $expired = date('Y-m-d H:i:s', time() - $lifetime);
    $sql = "DELETE FROM {$this->table} WHERE updated < '{$expired}'";
    return !!$this->db_exec($sql);
  }

  private function db() {
    return Model::build();
  }

  private function db_query($sql) {
    return $this->db()->query($sql);
  }

  private function db_first($sql) {
    $res = $this->db_query($sql);
    return isset($res[0]) ? $res[0] : null;
  }

  private function db_exec($sql) {
    return $this->db()->execute($sql);
  }

  private function db_escape($string) {
    return $this->db()->escape($string);
  }

}

?>
