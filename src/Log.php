<?php

class Log {

  public static function write($name, $line) {
    $filename = "../log/{$name}.log";
    $line = "\n=== " . date("Y-m-d\TH:i:sO") . "  " . $line;
    file_put_contents($filename, $line, FILE_APPEND);
  }
}