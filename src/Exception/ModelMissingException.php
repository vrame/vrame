<?php

namespace Vrame\Exception;

class ModelMissingException extends \Exception {
  protected $code = 401;
  protected $message = "Model `%s` not found.";

  public function __construct($modelName) {
    $this->message = sprintf($this->message, $modelName);
  }
}

?>
