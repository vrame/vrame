<?php

namespace Vrame;

abstract class ErrorHandler {
  function __construct($error_types=E_ERROR) {
    set_error_handler([$this, 'system_error'], $error_types);
  }
  public function system_error($code, $message, $file, $line) {
  }
  public function error($errorObject, $dispatcher) {
  }
  public function exception($exception, $dispatcher) {
  }
}

?>
