<?php

namespace Vrame;

use \Exception;

if (! defined('DS')) define('DS', DIRECTORY_SEPARATOR);

class APP {
  const TIMEZONE = 'UTC';

  public static $ROOT = '';
  public static $settings;

  private $config_data    = [];
  private $session        = null;
  private $output_buffer  = null;

  private static $instance;
  public static function getInstance() {
    if (! self::$instance) {
      self::$instance = new self();
    }

    return self::$instance;
  }

  public static function setInstance($app) {
    if ($app instanceof self) {
      self::$instance = $app;
    }
  }

  private function __construct($init=true) {
    self::$instance = $this;
    self::$ROOT = dirname(dirname(dirname(dirname(dirname(__FILE__)))));
    self::_include("/config/config.php");
    self::_include("/config/routes.php");

    if ($init) {
      $this->session = new SessionHandler();
      $this->output_buffer = new OutputBufferHandler();

      $this->session->start();
      $this->output_buffer->start();
    }

    $this->default_timezone();
  }

  public static function _include($path) {
    include_once self::$ROOT . $path;
  }

  private function default_timezone() {
    $timezone = self::config('timezone');
    date_default_timezone_set($timezone ? $timezone : self::TIMEZONE);
  }

  private function _require($path, $className) {
    $path = self::$ROOT . $path;var_dump($path);die;
    if (file_exists($path)) {
      require_once $path;
    } else {
      throw new Exception("{$className} not found.");
    }
  }

  public static function parseUrl($url) {
    $url = preg_replace('/^\/+|\/+$/', '', $url);
    $request = (object)array(
      'controller'  => 'index',
      'action'      => 'index',
      'params'      => array(),
      'named'       => array(),
      'method'      => strtolower($_SERVER['REQUEST_METHOD']),
      'protocol'    => strtolower($_SERVER['SERVER_PROTOCOL']),
      'client_addr' => $_SERVER['REMOTE_ADDR']
    );

    if ($r = Router::find($url)) {
      if (is_array($r)) {
        $request->controller  = $r['controller'];
        $request->action      = $r['action'];
        $request->params      = $r['params'];
        return $request;
      }

      $url = $r;
    }

    $tmp = explode("/", preg_replace('/^\/+|\/+$/', '', $url));
    foreach($tmp as $k=>$v) {
      if(preg_match('/^[a-z0-9]+:[^\/]*$/i', $v)) {
        list($name, $value) = explode(":", $v, 2);
        if(! empty($name) && !empty($value)) {
          $request->named[$name] = $value;
        }
        unset($tmp[$k]);
      }
    }

    if (! empty($tmp[0])) {
      $request->controller = $tmp[0];
    }

    if (! empty($tmp[1])) {
      $request->action = $tmp[1];
    }

    if (count($tmp)>2) {
      $request->params = array_slice($tmp, 2);
    }

    return $request;
  }

  public static function request2url($req) {
    $url = "/{$req->controller}";
    if ($req->action != 'index') {
      $url .= "/{$req->action}";
    }

    if (! empty($req->params)) {
      if ($req->action == 'index') {
        $url .= "/{$req->action}";
      }
      $url .= "/" . implode("/", $req->params);
    }

    return $url;
  }

  public static function settings($name, $locale=null) {
    if (! self::$settings) {
      if (! $locale) {
        if (! $locale = Locale::get()) {
          $langs = array_keys(Locale::languages());
          $locale = @$langs[0];
          Locale::set($locale);
        }
      }
      $path = implode(DS, array(self::$ROOT, "config", "settings" . ($locale ? ".{$locale}" : "") .".data"));
      if (file_exists($path)) {
        self::$settings = unserialize(file_get_contents($path));
      }
    }

    if ($name == '*') {
      $value = self::$settings;
    } else {
      foreach(explode(".", $name) as $n) {
        $value = isset($value) ? @$value[$n] : @self::$settings[$n];
      }
    }

    return $value;
  }

  public static function underscore($camelCase) {
    $underscore = strtolower(substr($camelCase, 0, 1));

    for ($i=1; $i<strlen($camelCase); $i++) {
      $c = substr($camelCase, $i, 1);
      if (preg_match('/[a-z]/i', $c) && strtoupper($c) == $c && !empty($underscore)) {
        $underscore .= "_".strtolower($c);
      } else {
        $underscore .= strtolower($c);
      }
    }
    return $underscore;
  }

  public static function urlize($string) {
    return mb_eregi_replace('[^a-z0-9čćšđž]+', '-', strtolower($string));
  }

  public static function camelcase($underscore) {
    return ucfirst(preg_replace_callback('/_(.)/', function($matches) {
      return strtoupper($matches[1]);
    }, $underscore));
  }

  public static function config($name, $value=null) {
    $inst = self::getInstance();

    if ($value === null) {
      if (strstr($name, ".")) {
        $items = explode(".", $name);
        $cfg = $inst->config_data;
        foreach($items as $k) {
          $cfg = $cfg[$k];
        }
        return $cfg;
      } else {
        return isset($inst->config_data[$name]) ? $inst->config_data[$name] : null;
      }
    } else {
      $inst->config_data[$name] = $value;
    }
  }

  public static function parseFileSize($string) {
    $string = preg_replace('/B/i', '', $string);
    $string = preg_replace('/K/i', ' * 1024', $string);
    $string = preg_replace('/M/i', ' * 1024 * 1024', $string);
    $string = preg_replace('/G/i', ' * 1024 * 1024 * 1024', $string);
    eval('$size = ' . $string . ';');
    return $size;
  }

  public static function humanizeFileSize($size) {

    if ($size >= 1024*1024*1024) {
      $size = ($size/(1024*1024*1024)) . "G";
    } elseif($size >= 1024*1024) {
      $size = ($size/(1024*1024)) . "M";
    } elseif($size >= 1024) {
      $size = ($size/1024) . "K";
    }

    return "{$size}B";
  }
}

?>
