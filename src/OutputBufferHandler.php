<?php

namespace Vrame;

class OutputBufferHandler {
  private $started = false;

  public function __construct() {
  }

  public function start() {
    if ($this->started) return;
    ob_start();
    $this->started = true;
  }
}

?>
