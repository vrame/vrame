<?php

namespace Vrame;

use Vrame\Locale,
    \Exception;

class Controller {

  public static $errors = array(
    500 => 'Server error.',
    404 => 'Page not found.'
    );

  public $autoRender = true;
  public $layout     = 'layout';
  public $request;

  private $View;
  public $rendered = false;

  public $safeActions = array();

  public function __construct($request) {
    $this->request = $request;

    $this->View = new View($this->request, $this);
  }

  public function beforeFilter() {

  }

  public function beforeRender() {

  }

  public function header($header) {
    header($header);
  }

  public function http_error($code, $message = 'Server Error', $content=null) {
    $this->header("HTTP/1.1 {$code} {$message}");

    if ($content === null) {
      if (APP::config('environment') != 'development') {
        $message = __(self::$errors[$code]);
      }
      $this->set('message', $message);
      $this->pageTitle = "Error {$code}";
      echo $this->View->render("errors/{$code}", 'error');
    } else {
      echo $content;
    }
    die;
  }

  public function render($view = null, $layout = null) {
    if ($layout === null) {
      $layout = $this->layout;
    }

    if ($view == null) {
      $view = $this->request->action;
    }

    $this->beforeRender();

    $ctrl = $this->request->controller;
    if (preg_match('/^\.\./', $view)) {
      $view = preg_replace('/^\.\.\//', '', $view);
      $ctrl = dirname($ctrl);
    }

    $ctrl = APP::camelcase($ctrl);
    $path = "{$ctrl}/{$view}";
    echo $this->View->render($path, $layout);
    $this->rendered = true;
  }
  public function renderText($text, $layout = null) {
    if ($layout === null) {
      $layout = $this->layout;
    }

    if ($layout) {
      $this->set('__content', $text);
      echo $this->View->fetch($layout);
    } else {
      echo $text;
    }
    $this->rendered = true;
  }

  public function renderJson($data) {
    $this->header("Content-Type: application/json; charset=utf-8");
    echo json_encode($data);
    die;
  }

  public function fetch($view) {
    return $this->View->fetch($view, false);
  }

  public function set($name, $value=null) {
    if (is_array($name) && $value === null) {
      foreach ($name as $k=>$v) {
        $this->View->set($k, $v);
      }
    } else {
      $this->View->set($name, $value);
    }
  }

  public function redirect($url) {
    ob_clean();
    $this->header('Location: ' . $url);
    exit;
  }

  public function requestAuthentication() {
    $user = @$_SESSION['auth_user'];
    $isSafe = in_array($this->request->action, $this->safeActions);
    if (!$user && !$isSafe) {
      $this->session('referer', APP::request2url($this->request));
      if (! method_exists($this, 'login')) {
        $error = "`login` method not implemented.";
        throw new Exception($error);
      }
      $this->login();
      die;
    }
  }

  public function authorize($session_id) {
    $this->session('auth_user', $session_id);
  }

  public function logout($redirectUrl = "/admin") {
    $this->session_delete('auth_user');
    $this->redirect($redirectUrl);
  }

  public function referer($default='/') {
    $ref = @$_SERVER['HTTP_REFERER'];
    return empty($ref) ? $default : $ref;
  }

  public function session($name, $value=null) {
    if ($value === null) {
      return @$_SESSION[$name];
    } else {
      $_SESSION[$name] = $value;
    }
  }

  public function session_delete($name) {
    unset($_SESSION[$name]);
  }

  protected function flash($message, $class='error', $timeout=5000) {
    $this->session('flash', array(
      'message' => $message,
      'class'   => $class,
      'timeout' => $timeout
      ));
  }

  protected function POST($name, $default=false) {
    return isset($_POST[$name]) ? $_POST[$name] : $default;
  }

  protected function GET($name, $default=false) {
    return isset($_GET[$name]) ? $_GET[$name] : $default;
  }

  protected function REQUEST($name, $default=false) {
    $ret = $this->GET($name, $default);
    if ($ret === $default) {
      $ret = $this->POST($name, $default);
    }

    return $ret;
  }

  protected function FILE($name, $default=false) {
    return isset($_FILES[$name]) ? $_FILES[$name] : $default;
  }

  protected function NAMED($name, $default=NULL) {
    $params = $this->request->named;
    return isset($params[$name]) ? $params[$name] : $default;
  }

  protected function isAjax() {
    return isset($_REQUEST['__ajax_req']) && $_REQUEST['__ajax_req'];
  }
}

?>
