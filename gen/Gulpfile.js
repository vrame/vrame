var g       = require('gulp');
var coffee  = require('gulp-coffee');
var uglify  = require('gulp-uglify');
var sass    = require('gulp-sass');

g.task('default', ['coffee', 'sass']);

g.task('coffee', function() {
  return g
    .src('assets/coffeescript/**/*.coffee')
    .pipe(coffee({bare: true}))
    .pipe(uglify())
    .pipe(g.dest('public/js/assets'));
});

g.task('sass', function() {
  return g
    .src('assets/sass/**/*.sass')
    .pipe(sass({
      outputStyle: 'compressed',
      includePaths: 'assets/sass/import'
    }))
    .pipe(g.dest('public/css/assets'));
});

g.task('watch', function() {
  g.watch('assets/coffeescript/**/*.coffee', ['coffee']);
  g.watch('assets/sass/**/*.sass', ['sass']);
});