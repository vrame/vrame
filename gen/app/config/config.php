<?php

use Vrame\APP;

$ENV = @parse_ini_file("environment.ini");
APP::config('ENV', $ENV);

// environment
APP::config('environment', preg_match('/^dev\./', $_SERVER['SERVER_NAME']) ? 'development' : 'production');

// database configuration
APP::config('db', parse_ini_file("database.ini"));


?>