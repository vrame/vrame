<?php

define("DS", DIRECTORY_SEPARATOR);

$self = array_shift($argv);

$HERE = dirname(__FILE__);
$ROOT = dirname($HERE);

$what = array_shift($argv);
if (empty($what)) {
  usage();
}

switch(strtolower($what)) {
  default:
    usage();

  case "app":
    $source = root_path("gen");
    $dest   = dirname(dirname(dirname($ROOT)));
    rcopy($source, $dest);
    echo "\nApplication is created.";
    break;

  case "skeleton":
    $name = array_shift($argv);
    if (empty($name)) {
      echo "Usage: {$self} skeleton NAME";
      die;
    }   

    $action = <<<ACTION
public function index() {}
ACTION;
    createcontroller($name, $action);
    createmodel($name);
    createview("{$name}", "index", $name);
    echo "\nSkeleton for `{$name}` is created.";
    break;

  case "controller":
    $name = array_shift($argv);
    if (empty($name)) {
      echo "Usage: {$self} controller NAME";
      die;
    }
    createcontroller($name);
    echo "\nController `{$name}` is created.";
    break;

  case "model":
    $name = array_shift($argv);
    if (empty($name)) {
      echo "Usage: {$self} model NAME";
      die;
    }
    createmodel($name);
    echo "\nModel `{$name}` is created.";
    break;

  case "view":
    $model  = array_shift($argv);
    $view   = array_shift($argv);
    if (empty($model) || empty($view)) {
      echo "Usage: {$self} view MODEL NAME";
      die;
    }
    createview($model, $view);
    echo "\nView `{$model}/{$view}` is created.";
    break;
}


function usage() {
  global $self;
  echo "Usage: {$self} OBJECT\n\n";
  echo "OBJECT:\tapp, skeleton, controller, model\n";
  die;
}

function rcopy($source, $dest) {
  if ($dir = @opendir($source)) {
    while ($f = readdir($dir)) {
      if (!preg_match('/^\.+$/', $f)) {
        if (is_dir("{$source}/{$f}")) {
          if (!is_dir("{$dest}/{$f}")) {
            @mkdir("{$dest}/{$f}");
          }
          rcopy("{$source}/{$f}", "{$dest}/{$f}");
        } else {
          copy("{$source}/{$f}", "{$dest}/{$f}");
        }
      }
    }

    closedir($dir);
  }

}

function createcontroller($name, $content="") {
  global $ROOT;
  $dir = path(".", "src", "Controller");
  if (!is_dir($dir)) {
    @mkdir($dir);
  }

  $name     = parseName($name);
  $filename = "{$name->camel}.php";
  $content  = <<<CONTENT
<?php

namespace App\Controller;

use Vrame\Controller;

class {$name->camel} extends Controller {
  {$content}
}

?>
CONTENT;

  file_put_contents(path(".", "src", "Controller", $filename), $content);
}

function createmodel($name) {
  global $ROOT;
  $dir = path(".", "src", "Model");
  if (!is_dir($dir)) {
    @mkdir($dir);
  }

  $name     = parseName($name);
  $filename = "{$name->camel}.php";
  $content  = <<<CONTENT
<?php

namespace App\Model;

use Vrame\Model;

class {$name->camel} extends Model {

}

?>
CONTENT;

  file_put_contents(path(".", "src", "Model", $filename), $content);
}

function createview($model, $view, $content="") {
  global $ROOT;

  $model  = parseName($model);
  $view   = parseName($view);
  $dir    = path(".", "src", "View", $model->camel);
  if (!is_dir($dir)) {
    @mkdir($dir);
  }

  $filename = path(".", "src", "View", $model->camel, "{$view->underscore}.ctp");
  file_put_contents($filename, $content);
}

function root_path() {
  global $ROOT;
  return $ROOT . DS . implode(DS, func_get_args());
}

function path() {
  return implode(DS, func_get_args());
}

function parseName($name) {
  $ret = new StdClass;
  $ret->original    = $name;
  $ret->camel       = preg_replace('/(?:^|_)(.?)/e',"strtoupper('$1')", $name);
  $ret->underscore  = strtolower(preg_replace('/([^A-Z])([A-Z])/', "$1_$2", $name));
  return $ret;
}

?>